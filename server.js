const express = require('express');
const Bundler = require('parcel-bundler');
const path = require('path');
const fs = require('fs');

const app = express();

//ROUTES
app.get('/', (req, res) => {
    const indexPage = path.resolve(__dirname, './dist/index.html');
    const indexPageContents = fs.readFileSync(indexPage, 'utf-8');
    res.send(indexPageContents);
});

app.use('/static', express.static(
    path.resolve(__dirname, './dist')
));

var server = app.listen(5000, function() {
    console.log("server is running at http://localhost:" + server.address().port);
  });